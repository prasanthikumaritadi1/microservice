package com.greatlearning.departmentservice.service;

import com.greatlearning.departmentservice.entity.Department;
import com.greatlearning.departmentservice.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department) {
        log.info("Inside SaveDepartment method of DepartmentService");
        return departmentRepository.save(department);
    }

    public Department findDepartmentByDepartmentId(Long departmentId) {
        log.info("Inside findDepartmentById method of DepartmentService");
        return departmentRepository.findDepartmentByDepartmentId(departmentId);
    }
}
